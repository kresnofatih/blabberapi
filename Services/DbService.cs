using BlabberApi.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BlabberApi.Services
{
    public static class DbService
    {
        public static void AddAppDbService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(opt=>
                opt.UseNpgsql(configuration.GetConnectionString("myPostgresDbConnection"))
            );
        }
    }
}