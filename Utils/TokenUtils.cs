using System;
using System.Collections.Generic;
using BlabberApi.Constants;
using JWT.Algorithms;
using JWT.Builder;

namespace BlabberApi.Utils
{
    public static class TokenUtils
    {
        public static string GenerateToken(string userId, string cls)
        {
            var token = JwtBuilder.Create()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(TokenConstants.Secret)
                .AddClaim("exp", DateTime.UtcNow.AddMinutes(5).ToFileTimeUtc())
                .AddClaim("uid", userId)
                .AddClaim("cls", cls)
                .Encode();
            return token;
        }

        public static IDictionary<string, object> ExtractClaims(this string token)
        {
            IDictionary<string, object> dict = null;
            try
            {
                dict = JwtBuilder.Create()
                    .WithAlgorithm(new HMACSHA256Algorithm())
                    .WithSecret(TokenConstants.Secret)
                    .MustVerifySignature()
                    .Decode<IDictionary<string, object>>(token);
            }
            catch
            {
                //
            }
            return dict;
        }

        public static object ExtractClaim(string token, string key)
        {
            var dict = token.ExtractClaims();
            if(!dict[key].isNull())
            {
                return dict[key];
            }
            return null;
        }

        public static bool ValidateExpiry(this IDictionary<string, object> dict)
        {
            var expiryTime = dict["exp"];
            if(!expiryTime.isNull())
            {
                bool isValid = (long) expiryTime > DateTime.UtcNow.ToFileTimeUtc();
                return isValid;
            }
            return false;
        }

        public static bool ValidateClaims(this string token)
        {
            var dict = token.ExtractClaims();
            if(dict.isNull())
            {
                return false;
            }
            var isValid = dict.ValidateExpiry();
            return isValid;
        }

        public static bool ValidateClaims(this string token, string validationParamsType)
        {
            var dict = token.ExtractClaims();
            if(dict.isNull())
            {
                return false;
            }

            var isValid = dict.ValidateExpiry();
            if(!isValid)
            {
                return false;
            }

            foreach(var item in AuthValidationParams.GetByType(validationParamsType))
            {
                if((string) dict[item.Key] != item.Value)
                {
                    isValid = false;
                    break;
                }
            }

            return isValid;
        }

        public static string RefreshToken(this string token)
        {
            string uid = (string) ExtractClaim(token, TokenConstants.ClaimTypes.Uid);
            string cls = (string) ExtractClaim(token, TokenConstants.ClaimTypes.Cls);
            return GenerateToken(uid, cls);
        }
    }
}