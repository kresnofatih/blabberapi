using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlabberApi.Models
{
    public class Relation
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string RelationId {get; set;}

        [Required]
        [MaxLength(30)]
        public string UserId {get; set;}

        [Required]
        [MaxLength(30)]
        public string ToUserId {get; set;}

        [Required]
        [MaxLength(20)]
        public string Relationship {get; set;}

        [Required]
        public DateTime CreatedAt {get; set;}
    }
}