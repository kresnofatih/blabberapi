using System.ComponentModel.DataAnnotations;

namespace BlabberApi.Models.DTOs.RelationDTOs
{
    public class NewRelationReq
    {
        [Required]
        [MaxLength(30)]
        public string UserId {get; set;}

        [Required]
        [MaxLength(30)]
        public string ToUserId {get; set;}

        [Required]
        [MaxLength(20)]
        public string Relationship {get; set;}
    }
}