using System.ComponentModel.DataAnnotations;

namespace BlabberApi.Models.DTOs.UserDTOs
{
    public class LogUserReq
    {
        [Required]
        [MaxLength(30)]
        public string UserId {get; set;}

        [Required]
        public string Password {get; set;}
    }
}