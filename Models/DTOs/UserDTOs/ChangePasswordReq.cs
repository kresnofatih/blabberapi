using System.ComponentModel.DataAnnotations;

namespace BlabberApi.Models.DTOs.UserDTOs
{
    public class ChangePasswordReq
    {
        [Required]
        public string OldPassword {get; set;}

        [Required]
        public string NewPassword {get; set;}
    }
}