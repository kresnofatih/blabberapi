using System.ComponentModel.DataAnnotations;

namespace BlabberApi.Models.DTOs.UserDTOs
{
    public class UpdateUserReq
    {
        [Required]
        [MaxLength(30)]
        public string FullName {get; set;}

        [MaxLength(100)]
        public string Bio {get; set;}

        public string PhotoUrl {get; set;}
    }
}