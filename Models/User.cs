using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BlabberApi.Models
{
    public class User
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [MaxLength(30)]
        public string UserId {get; set;}

        [Required]
        [MaxLength(30)]
        public string FullName {get; set;}

        [MaxLength(100)]
        public string Bio {get; set;}

        public string PhotoUrl {get; set;}

        [Required]
        [MaxLength(20)]
        public string Class {get; set;}

        [Required]
        [JsonIgnore]
        public string HashedPassword {get; set;}

        [Required]
        public DateTime DateOfBirth {get; set;}

        [Required]
        public DateTime CreatedAt {get; set;}
    }
}