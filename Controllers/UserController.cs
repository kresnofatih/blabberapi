using System.Collections.Generic;
using System.Threading.Tasks;
using BlabberApi.Attributes;
using BlabberApi.Constants;
using BlabberApi.Controllers.Functions;
using BlabberApi.Data;
using BlabberApi.Models;
using BlabberApi.Models.DTOs.UserDTOs;
using BlabberApi.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BlabberApi.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private AppDbContext _appDbContext;

        private UserFunctions _userFunctions;

        public UserController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            _userFunctions = new UserFunctions(appDbContext);
        }

        [HttpPost]
        public async Task<ActionResult> CreateUser([FromBody]NewUserReq newUserReq)
        {
            var createdUser = await _userFunctions.FunctionCreateUser(newUserReq);
            if(createdUser.isNull())
            {
                return BadRequest("not created");
            }
            return Ok();
        }

        [HttpPost("login")]
        public async Task<ActionResult> LogUser([FromBody]LogUserReq logUserReq)
        {
            var token = await _userFunctions.FunctionLogUser(logUserReq);
            if(token.isNull())
            {
                return BadRequest("not logged");
            }
            Response.Headers.Add(HeaderTypes.AccessToken, token);
            return Ok();
        }
        
        [Auth(AuthValidationParams.Types.MasterClassOnly)]
        [HttpGet("get-all")]
        public ActionResult<IEnumerable<User>> GetAllUsers()
        {
            var res = _userFunctions.FunctionGetAllUsers();
            return Ok(res);
        }

        [Auth]
        [HttpGet("get-one/{userId}")]
        public async Task<ActionResult<User>> GetUserById(string userId)
        {
            var res = await _userFunctions.FunctionGetUserById(userId);
            if(res.isNull())
            {
                return BadRequest("not found");
            }
            return Ok(res);
        }

        [Auth]
        [HttpPut("update")]
        public async Task<ActionResult<User>> UpdateUser([FromBody]UpdateUserReq updates)
        {
            var token = Request.Headers[HeaderTypes.AccessToken];
            string userId = (string) TokenUtils.ExtractClaim(token, TokenConstants.ClaimTypes.Uid);
            var res = await _userFunctions.FunctionUpdateUser(updates, userId);
            if(res.isNull())
            {
                return BadRequest("not updates");
            }
            return Ok(res);
        }

        [Auth]
        [HttpPut("change-password")]
        public async Task<ActionResult<bool>> ChangePassword([FromBody]ChangePasswordReq pwdUpdates)
        {
            var token = Request.Headers[HeaderTypes.AccessToken];
            string userId = (string) TokenUtils.ExtractClaim(token, TokenConstants.ClaimTypes.Uid);
            var pwdIsUpdated = await _userFunctions.FunctionUpdatePassword(userId, pwdUpdates);
            if(pwdIsUpdated)
            {
                return Ok();
            }
            return BadRequest("no password change");
        }

        [Auth(AuthValidationParams.Types.MasterClassOnly)]
        [HttpPut("classify-as-master/{userId}")]
        public async Task<ActionResult<bool>> UpgradeToMaster(string userId)
        {
            bool upgradedToMaster = await _userFunctions.FunctionUpgradeToMaster(userId);
            if(upgradedToMaster)
            {
                return Ok();
            }
            return BadRequest("not upgraded");
        }

        [Auth(AuthValidationParams.Types.MasterClassOnly)]
        [HttpPut("classify-as-basic/{userId}")]
        public async Task<ActionResult<bool>> UpgradeToBasic(string userId)
        {
            bool upgradedToBasic = await _userFunctions.FunctionUpgradeToBasic(userId);
            if(upgradedToBasic)
            {
                return Ok();
            }
            return BadRequest("not upgraded");
        }

        [Auth(AuthValidationParams.Types.MasterClassOnly)]
        [HttpPut("classify-as-suspended/{userId}")]
        public async Task<ActionResult<bool>> DowngradeToSuspended(string userId)
        {
            bool isDowngraded = await _userFunctions.FunctionDowngradeToSuspended(userId);
            if(isDowngraded)
            {
                return Ok();
            }
            return BadRequest("not suspended");
        }

        [Auth(AuthValidationParams.Types.MasterClassOnly)]
        [HttpDelete("delete/{userId}")]
        public async Task<ActionResult<bool>> DeleteUser(string userId)
        {
            var deleted = await _userFunctions.FunctionDeleteUser(userId);
            return Ok(deleted);
        }
    }
}