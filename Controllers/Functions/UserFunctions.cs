using System.Collections.Generic;
using System.Threading.Tasks;
using BlabberApi.Constants;
using BlabberApi.Data;
using BlabberApi.Models;
using BlabberApi.Models.DTOs.UserDTOs;
using BlabberApi.Models.Methods;
using BlabberApi.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BlabberApi.Controllers.Functions
{
    [NonController]
    public class UserFunctions : ControllerBase
    {
        public AppDbContext _appDbContext;

        public UserFunctions(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<User> FunctionGetUserById(string userId)
        {
            return await _appDbContext.Users.FindAsync(userId);
        }

        public IEnumerable<User> FunctionGetAllUsers()
        {
            return _appDbContext.Users;
        }

        public async Task<User> FunctionCreateUser(NewUserReq newUserReq)
        {
            var userExistsResult = await GetUserExists(newUserReq.UserId);
            if(userExistsResult.Exists)
            {
                return null;
            }
            var newUser = newUserReq.toNewUser();
            await _appDbContext.Users.AddAsync(newUser);
            await _appDbContext.SaveChangesAsync();
            return newUser;
        }

        public async Task<User> FunctionUpdateUser(UpdateUserReq updateUserReq, string userId)
        {
            var userExistsResult = await GetUserExists(userId);
            if(!userExistsResult.Exists || !userExistsResult.IsNotSuspended)
            {
                return null;
            }
            userExistsResult.User.updateUser(updateUserReq);
            await _appDbContext.SaveChangesAsync();
            return userExistsResult.User;
        }

        public async Task<bool> FunctionUpdatePassword(string userId, ChangePasswordReq pwdUpdates)
        {
            var userExistsResult = await GetUserExists(userId);
            if(!userExistsResult.Exists || !userExistsResult.IsNotSuspended)
            {
                return false;
            }
            bool oldPasswordsMatch = pwdUpdates.OldPassword.verifyPassword(userExistsResult.User.HashedPassword);
            if(oldPasswordsMatch)
            {
                userExistsResult.User.HashedPassword = pwdUpdates.NewPassword.toHashedPassword();
                await _appDbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> FunctionDeleteUser(string userId)
        {
            var userExistsResult = await GetUserExists(userId);
            bool deleted = false;
            if(userExistsResult.Exists)
            {
                _appDbContext.Users.Remove(userExistsResult.User);
                _appDbContext.SaveChanges();
                deleted = true;
            }
            return deleted;
        }

        public async Task<string> FunctionLogUser(LogUserReq logUserReq)
        {
            var userExistsResult = await GetUserExists(logUserReq.UserId);
            if(userExistsResult.Exists && userExistsResult.IsNotSuspended)
            {
                bool passwordIsMatch = logUserReq.Password.verifyPassword(userExistsResult.User.HashedPassword);
                if(passwordIsMatch)
                {
                    return TokenUtils.GenerateToken(logUserReq.UserId, userExistsResult.User.Class);
                }
                return null;
            }
            return null;
        }

        public async Task<bool> FunctionUpgradeToMaster(string userId)
        {
            var userExistsResult = await GetUserExists(userId);
            if(userExistsResult.Exists)
            {
                userExistsResult.User.Class = UserClasses.Master;
                await _appDbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> FunctionUpgradeToBasic(string userId)
        {
            var userExistsResult = await GetUserExists(userId);
            if(userExistsResult.Exists)
            {
                userExistsResult.User.Class = UserClasses.Basic;
                await _appDbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> FunctionDowngradeToSuspended(string userId)
        {
            var userExistsResult = await GetUserExists(userId);
            if(userExistsResult.Exists)
            {
                userExistsResult.User.Class = UserClasses.Suspended;
                await _appDbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<UserExistsObject> GetUserExists(string userId)
        {
            var user = await FunctionGetUserById(userId);
            if(!user.isNull())
            {
                if(user.Class != UserClasses.Suspended)
                {
                    return new UserExistsObject(true, true, user);
                }
                return new UserExistsObject(true, false, user);
            }
            return new UserExistsObject(false, false, null);
        }
    }

    public class UserExistsObject
    {
        public UserExistsObject(bool exists, bool isNotSuspended, User user)
        {
            Exists = exists;
            User = user;
            IsNotSuspended = isNotSuspended;
        }
        public bool Exists {get; set;}

        public bool IsNotSuspended {get; set;}

        public User User {get; set;}
    }
}