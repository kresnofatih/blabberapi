namespace BlabberApi.Constants
{
    public static class RelationshipTypes
    {
        // when connected relation exists for a user
        // a peer connected relation corresponding relation must exist
        public static readonly string Connected = "Connected";
        
        // only the blocker user can still see the 'blocked' user
        public static readonly string Blocked = "Blocked";

        // if hasn't been any relation at all between two users
        // and a user wants to initiate connection, he/she will start by
        // the 'pending' connection
        public static readonly string Pending = "Pending";
    }
}